# watch_all.php

Un script pour s'inscrire (watch) à tous les projets de git.spip.net

## Installation:
- s'assurer que php est disponible dans le PATH de la machine
- coller une copie de watch_all.php dans un dossier quelconque mais avec droits d'écriture.

## Utilisation:
Pour l'utiliser, il faut d'abord créer un token depuis son compte.

Dans Configuration -> Applications, donnez un nom au jeton et validez. Le jeton sera affiché juste au dessus, copiez/collez le, il ne sera plus affiché ensuite.

Lancez le script avec :

```
php watch_all.php token
```

Le script indique d'abord le nombre de repos auxquels on est inscrit, puis appelle l'API pour chaque orga et chaque repo auquel on n'est pas encore inscrit.

PS : merci à Cédric pour les fonctions du débardeur que j'ai reprises et adaptées